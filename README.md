# Technical test @ Luko

The purpose of this test is to assess your coding level and your ML and statistical knowledge to see if it fits our standards. The test is composed of three parts. It should take you around 4 hours to complete it.

You should always looked at the #FIXME comment

# Part one : jupyter notebook and pandas

The purpose of this test is for you to install all the necessary tools and packages to run the jupyter notebook *jupyter_and_pandas.ipynb* and then to follow the instructions on the notebook. If some questions ask you to explain something, write your answers in markdown blocks.

# Part two : ML oriented notebook

This part is more focused on your knowledge of machine learning and statistics. Follow the jupyter notebook *ml_oriented.ipynb* and write your answers in markdown blocks.

# What will we look at?

 * The jupyter notebooks and the explanations in it
 * The code architecture and content
 * The code comments and documentation

# What's after?

When you are done with your project, fill the make rule *release* and use it to make a zipfile of your whole project excluding the datasets and the .git folder. Then, send us by email the result.

We will contact you in the following days to debrief your work.

If you have any quesion during the exercices, do not hesitate to contact us directly.
